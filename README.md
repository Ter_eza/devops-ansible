# Ansible – Ukázkový projekt

Tento ukázkový Ansible projekt nasazuje aplikaci Netdata vč. reverzní proxy (Apache2).

Playbooky:

- `play_example.yml` – vytvoří dočasný soubor s daným obsahem
- `play_install.yml` – nasadí proxy a Netdata

Role:

- `roles/netdata` – nainstaluje aplikaci Netdata
- `roles/proxy` – nainstaluje Apache2 a nakonfiguruje v něm reverzní proxy pro přístup k aplikaci Netdata

Inventáře:

- `inventories/local.yml` – lokální prostředí (localhost)
- `inventories/test.yml` – testovací prostředí (virtuální server, připojení jako `ssh -p 2222 devops@localhost`)
